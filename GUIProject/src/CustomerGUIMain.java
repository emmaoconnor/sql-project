import java.sql.SQLException;

import javax.swing.JFrame;

public class CustomerGUIMain {

	public static void main(String[] args) throws SQLException {
		CustomerGUI customer = new CustomerGUI();
		customer.setVisible(true);
		customer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		customer.setSize(800, 500);

	}

}
