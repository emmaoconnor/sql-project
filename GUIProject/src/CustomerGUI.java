import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Connection;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

//import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * This class is the main window for Customer program. It demonstrates the components, layout, Action Listeners, methods and database connection showing data in a JTable.
 * 
 * @author Emma
 *
 */
public class CustomerGUI extends JFrame{
	
    private FlowLayout layout; // layout object
    String[] columnNames = {"CustomerID","First Name","LastName", "Phone", "Address"};
    Object[][] data; 
    DefaultTableModel dm ;
    JTabbedPane tabbedPane;
	
    /**
	 * The JButtons are created.
	 *
	 */
	JButton createButton = new JButton();
	JButton deleteButton = new JButton();
	JButton updateButton = new JButton();
	JButton exitButton = new JButton();
	JButton searchButton = new JButton();
	
	/**
	 * The JPanel for the CustomerGUI is created
	 * 
	 */
	JPanel panel;
	
	/**
	 * The JFrame when querying the database is created here.
	 * 
	 */
	JFrame frame1;
	
	/**
	 * The JTextFields are created.
	 * 
	 */
	JTextField fName = new JTextField();
	JTextField lName = new JTextField();
	JTextField phone = new JTextField();
	JTextField addr = new JTextField();
	JTextField searchfield = new JTextField();
	
	/**
	 * The JLabels are created.
	 * 
	 */
	JLabel firstnamelabel;
	JLabel lastnamelabel;
	JLabel phonelabel;
	JLabel addresslabel;
	JLabel title;
	JLabel searchlabel;
	
	/**
	 * The constructor creates a simple window.
	 * @throws SQLException Thrown when the connection to the database fails.
	 */
	public CustomerGUI () throws SQLException {
		super("Customer");
		setSize(800, 500);
		setLayout(null);
		title = new JLabel("CUSTOMER");
		title.setBounds(100, 20, 200, 30);
		

		/**
		 * The JLabels are given their titles and locations here.
		 * 
		 */
        firstnamelabel = new JLabel("Name:"); 
        firstnamelabel.setBounds(30, 85, 60, 30);
        lastnamelabel = new JLabel("Surname:"); 
        lastnamelabel.setBounds(30, 120, 60, 30);
        phonelabel = new JLabel("Phone:"); 
        phonelabel.setBounds(30, 155, 60, 30); 
        addresslabel = new JLabel("Address:"); 
        addresslabel.setBounds(30, 190, 60, 30);
        searchlabel = new JLabel("Search by Name:");
        searchlabel.setBounds(360, 400, 100, 30);
        
        /**
    	 * The JLabels are added to the panel.
    	 * 
    	 */
        add(title);
        add(firstnamelabel);
        add(lastnamelabel);
        add(phonelabel);
        add(addresslabel);
        add(searchlabel);
        
        /**
    	 * The buttons are given their titles and locations.
    	 * 
    	 */
        exitButton = new JButton("Exit"); 
        exitButton.setBounds(25, 250, 80, 25);            

        // Defining Register Button
        createButton = new JButton("Add");
        createButton.setBounds(130, 250, 80, 25);

        // Defining Update Button
        updateButton = new JButton("Update");
        updateButton.setBounds(130, 285, 80, 25);

        // Defining Delete Button
        deleteButton = new JButton("Delete");
        deleteButton.setBounds(25, 285, 80, 25);  
        
        searchButton = new JButton("Search");
        searchButton.setBounds(600, 402, 80, 25);

        
        /**
    	 * Action Listeners are allocated to the buttons
    	 * 
    	 */
        ListenForAction actionListener = new ListenForAction();
        createButton.addActionListener(actionListener);
        deleteButton.addActionListener(actionListener);
        updateButton.addActionListener(actionListener);
        exitButton.addActionListener(actionListener);
        searchButton.addActionListener(actionListener);
        
        /**
    	 * buttons are then added to the frame
    	 * 
    	 */
        add(exitButton);
        add(createButton);
        add(updateButton);
        add(deleteButton);
        add(searchButton);
        
        /**
    	 * The panel is set as a GridLayout() along with it's location and border before being added to the frame
    	 * 
    	 */
        panel = new JPanel();
        panel.setLayout(new GridLayout());
        panel.setBounds(250, 20, 480, 330);
        panel.setBorder(BorderFactory.createEtchedBorder());
        add(panel);

        /**
    	 * The textfields are given their names and locations
    	 * 
    	 */
        fName = new JTextField(); 
        fName.setBounds(95, 85, 130, 30);
        lName = new JTextField(); 
        lName.setBounds(95, 120, 130, 30);
        phone = new JTextField(); 
        phone.setBounds(95, 155, 130, 30);
        addr = new JTextField(); 
        addr.setBounds(95, 190, 130, 30);  
        searchfield = new JTextField();
        searchfield.setBounds(470, 400, 125, 30);
        
      //Adding textfields to panel
        add(fName);
        add(lName);
        add(phone);
        add(addr);
        add(searchfield);
        
        getData();
        
        /**
    	 * The table is displayed in the center of the screen
    	 * 
    	 */
        Dimension dim = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2,dim.height/2-this.getSize().height/2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
       

	       
        
        
       }
	
	/*-------------------------TABLE-----------------------------------------*/
        public void getData()  {
   
        	// JDBC driver name and database URL                              
            final String JDBC_DRIVER = "com.mysql.jdbc.Driver";        
            final String DATABASE_URL = "jdbc:mysql://localhost/cims";
           Connection connection = null; // manages connection
           Statement statement = null; // query statement
        
           try {
              // establish connection to database
              connection = (Connection) DriverManager.getConnection(DATABASE_URL, "root", "carl0w" );
              statement = (Statement) connection.createStatement();         
              
              ResultSet resultSet = statement.executeQuery("SELECT * FROM customer");
              // process query results
              ResultSetMetaData metaData = resultSet.getMetaData();
              int numberOfColumns = metaData.getColumnCount(); 
             data = new Object[100][numberOfColumns];
              int j=0,k=0;
              while ( resultSet.next() ) {
         for ( int i = 1; i <=numberOfColumns; i++){
         	data[j][k] = resultSet.getObject(i);
         	k++;
         }
         k=0; j++;
              } //end while
              
              
    
     dm = new DefaultTableModel(data, columnNames) ;
    //Adding object of DefaultTableModel into JTable
     JTable table = new JTable(dm);
    
     table.getTableHeader().setBackground(Color.ORANGE);
     table.getTableHeader().setForeground(Color.WHITE);
     table.setPreferredScrollableViewportSize(new Dimension(500, 100));
    
     JScrollPane scrollPane = new JScrollPane(table);
   
     panel.add(scrollPane); 
           }  // end try
           catch ( SQLException sqlException )  {
              sqlException.printStackTrace();
              System.exit( 1 );
           } // end catch
           finally // ensure statement and connection are closed properly
           {                                                             
              try {                                                          
                 statement.close();                                      
                 connection.close();                                     
              } // end try                                               
              catch ( Exception exception ) {                                                          
                 exception.printStackTrace();                                     
                 System.exit( 1 );                                       
              } // end catch                                             
           } // end finally                  
        }//end getData       
        
        
/*-------------------------------ACTION LISTENERS-------------------------------------------------*/
        
		private class ListenForAction implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				/**
				 * The add operation when button is clicked, A new record is inserted into the database
				 * @throws SQLException Thrown when the connection to the database fails.
				 */
				if(e.getSource() == createButton) {
					
					String firstname = fName.getText();
					String lastname = lName.getText();
					String phoneno = phone.getText();
					String address = addr.getText();
					
					if(firstname.equals("") || lastname.equals("") || phoneno.equals("") || address.equals("")) {
						JOptionPane.showMessageDialog(null, "Please provide data");	
					}
					
					else {
						CustomerTable customer = new CustomerTable();
						try {
							customer.addCustomer(firstname, lastname, phoneno, address);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						JOptionPane.showMessageDialog(null, "Customer was successfully inserted!");
						dispose();
						setVisible(false);
						try {
							new testing();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
						//set text fields to empty
						fName.setText("");
						lName.setText("");
						phone.setText("");
						addr.setText("");
						
					}
			}
				/**
				 * The delete operation when button is clicked. This will delete a record when a name is entered
				 * @throws SQLException Thrown when the connection to the database fails.
				 */
				else if(e.getSource() == deleteButton) {
					String firstname = fName.getText();
					
					if(firstname.equals("")) {
						JOptionPane.showMessageDialog(null, "Please provide data");
					}
					else {
						CustomerTable customer = new CustomerTable();
						int answer;
						answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this record?");
						if(answer == 0) {
						try {
							customer.deleteCustomer(firstname);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						JOptionPane.showMessageDialog(null, "Customer was successfully deleted!");
						
						dispose();
						setVisible(false);
						try {
							new testing();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
						//set text fields to empty
						fName.setText("");
						lName.setText("");
						phone.setText("");
						addr.setText("");
						}
						else if(answer > 0){
							JOptionPane.showMessageDialog(null, "Customer was not deleted");
						}
						
					}
				}
			
				/**
				 * The update operation when button is clicked. Database will update record's lastname when firstname given
				 * @throws SQLException Thrown when the connection to the database fails.
				 */
					else if(e.getSource() == updateButton) {
						String firstname = fName.getText();
						String lastname = lName.getText();
						
						if(firstname.equals("") || lastname.equals("")) {
							JOptionPane.showMessageDialog(null, "Please provide data");
						}
						else {
							CustomerTable customer = new CustomerTable();
							int answer;
							answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to update this record?");
							if(answer == 0) {
							try {
								customer.updateCustomer(firstname, lastname);
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
							JOptionPane.showMessageDialog(null, "Customer was successfully updated!");
							
							dispose();
							setVisible(false);
							try {
								new testing();
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
							
							//set text fields to empty
							fName.setText("");
							lName.setText("");
							phone.setText("");
							addr.setText("");
							}
							else if(answer > 0){
								JOptionPane.showMessageDialog(null, "*ERROR* Customer was not updated");
							}
						}
						
						/**
						 * The search operation when button is clicked. This will search the database when name is given in textfield
						 * 
						 */
				}  else if(e.getSource() == searchButton) {
					System.out.println("Showing table data results...");
					showTableData();
				 
				}
				
				/**
				 * The exit operation when button is clicked, The system will return to the Main Menu
				 * 
				 */
				else if (e.getSource() == exitButton) {
					new MainMenu();
					setVisible(false);
				}
					
			}
			
		
						
	}//end ActionListener class
		
	/*--------------------------------SEARCHING DATABASE METHOD----------------------------------------*/
		/**
		 * The method showTableData() is the method for querying the database. A text field is used to take in a name and open a new frame with the record in it producing the query.
		 * 
		 */
		public void showTableData() {
			frame1 = new JFrame("Database Search Result");
			frame1.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			frame1.setLayout(new BorderLayout());
			
			DefaultTableModel model = new DefaultTableModel();
			model.setColumnIdentifiers(columnNames);
			
			JTable table = new JTable();
			table.setModel(model);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			table.setFillsViewportHeight(true);
			
			JScrollPane scroll = new JScrollPane(table);
			scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			
			/**
			 * firstname is assigned to the search field text field
			 * 
			 */
			String firstname = searchfield.getText();
			String lastname = "";
			String phone = "";
			String address = "";
			
			if(searchfield.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Please provide a name to search the database.");
				frame1.setVisible(false);
			}
			else {
			try {
				final String DATABASE_URL = "jdbc:mysql://localhost/cims";
				Connection connection = null;
				Statement statement = null;
				ResultSet resultSet = null;
				connection = (Connection) DriverManager.getConnection(DATABASE_URL, "root", "carl0w");
				// create Statement for querying table
				statement = (Statement) connection.createStatement();
				// query database
				resultSet = statement.executeQuery("SELECT CustomerID, FirstName, LastName, PhoneNumber, Address FROM customer WHERE FirstName LIKE'"+firstname+"%'");
				// process query results
				ResultSetMetaData metaData = resultSet.getMetaData();
				int i = 0;
				if(resultSet.next()) {
					int id = resultSet.getInt("CustomerID");
					firstname = resultSet.getString("FirstName");
					lastname = resultSet.getString("LastName");
					phone = resultSet.getString("PhoneNumber");
					address = resultSet.getString("Address");
					model.addRow(new Object[] {id, firstname, lastname, phone, address});
					i++;
				}
				if(i<1) {
					JOptionPane.showMessageDialog(null, "No record found", "Error", JOptionPane.ERROR_MESSAGE);
					frame1.setVisible(false);
				}
				if(i==1) {
					System.out.println(i + " Record found");
				}
				else {
					System.out.println(i + " Records found");
				}
			} catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			frame1.add(scroll);
			frame1.setVisible(true);
			frame1.setSize(400, 300);
			
			}//end else
		}//end method	
			
}//end class
		
	

	
	
