import java.awt.Dimension;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * The CustomerTable contains the CRUD operations connecting to and querying the MySQL database
 * @author Emma
 */
public class CustomerTable {
	
	// database URL
		final String DATABASE_URL = "jdbc:mysql://localhost/cims";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		/**
		 * This will insert a single customer into the database.
		 * @param firstname, lastname, phonenumber and address are all values considered before inserting them into database.
		 * @throws SQLException Thrown when the connection to the database fails.
		 * @
		 */
		
		public void addCustomer(String firstname, String lastname, String phonenumber, String address) throws SQLException {
			
		connection = DriverManager.getConnection(DATABASE_URL, "root", "carl0w" );
		PreparedStatement pstat = connection.prepareStatement("INSERT INTO customer (FirstName, LastName, PhoneNumber, Address) VALUES (?,?,?,?)");
		pstat.setString (1, firstname);
		pstat.setString (2, lastname);
		pstat.setString (3, phonenumber);
		pstat.setString (4, address);
		pstat.executeUpdate();
			
		}

		/**
		 * This will delete a single customer.
		 * @param firstname Provide the first name of the customer you want to delete.
		 * @throws SQLException Thrown when the connection to the database fails.
		 */
	public void deleteCustomer(String firstname) throws SQLException {
		final String DATABASE_URL = "jdbc:mysql://localhost/cims";
	
			try{
			// establish connection to database
			connection = DriverManager.getConnection(DATABASE_URL, "root", "carl0w" );
			// create Statement for deleting from table
			statement = connection.createStatement();
			statement.executeUpdate("DELETE FROM customer WHERE FirstName='"+firstname+"' ");
			}
			catch(SQLException sqlException ) {
			sqlException . printStackTrace () ;
			}
			finally {
			try{
			statement. close () ;
			connection. close () ;
			}
			catch ( Exception exception ){
			exception.printStackTrace () ;
			}
			}
		
	}
	
	/**
	 * This will update a single customer.
	 * @param firstname, lastname Provide the first name and lastname of the customer you want to delete.
	 * @throws SQLException Thrown when the connection to the database fails.
	 */
	public void updateCustomer(String firstname, String lastname) throws SQLException {
		final String DATABASE_URL = "jdbc:mysql://localhost/cims";
		try{
		
			// establish connection to database
			connection = DriverManager.getConnection(DATABASE_URL, "root", "carl0w" );
			// create Statement for updating table
			statement = connection.createStatement();
			statement.executeUpdate("UPDATE customer SET LastName='"+lastname+"'WHERE FirstName='"+firstname+"' ");
			}
			catch(SQLException sqlException ) {
			sqlException . printStackTrace () ;
			}
			finally {
			try{
			statement. close () ;
			connection. close () ;
			}
			catch ( Exception exception ){
			exception . printStackTrace () ;
			}
			}
	}
	
	/**
	 * This will display the customer.
	 * 
	 * @throws SQLException Thrown when the connection to the database fails.
	 */
	public void displayCustomer() throws SQLException {
		try{
		
		// establish connection to database
		connection = DriverManager.getConnection(DATABASE_URL, "root", "carl0w");
		// create Statement for querying table
		statement = connection.createStatement();
		// query database
		resultSet = statement.executeQuery("SELECT CustomerID, FirstName, LastName, PhoneNumber, Address FROM customer");
		// process query results
		ResultSetMetaData metaData = resultSet.getMetaData();
		int numberOfColumns = metaData.getColumnCount();
		//System.out.println( "Customer Table Database:\n" );
		for ( int i = 1; i <= numberOfColumns; i++ )
		System.out.print(metaData.getColumnName(i) + "\t");
		System.out.println() ;
		
		while(resultSet.next()){
		for(int i = 1; i <= numberOfColumns; i++)
		System.out.print ( resultSet.getObject(i) + "\t\t");
		System.out.println() ;
		}
		}
		catch(SQLException sqlException) {
		sqlException.printStackTrace() ;
		}
		finally {
		try{
		resultSet.close();
		statement.close();
		connection.close();
		}
		catch(Exception exception){
		exception.printStackTrace() ;
		}
		}

	}
	
		
		
		
	
	
}
