import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


/**
 * This is the Main Menu for the system
 * @author Emma
 * 
 */
public class MainMenu implements ActionListener{
	
	JFrame frame;
	JButton customer;
	JButton product;
	JButton invoice;
	JPanel panel;
	JLabel label;
	
	/**
	 * The constructor creates a simple menu window.
	 */
	public MainMenu()
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		label = new JLabel("Customer Invoice Management System");
		label.setBackground(Color.BLUE);
		
		
		JPanel panel = new JPanel(new BorderLayout());
		JPanel north = new JPanel(new GridLayout(0,1,5,5));
        north.add(new JLabel("Main Menu", SwingConstants.CENTER));
        north.setBackground(Color.BLUE);
        north.setForeground(Color.WHITE);
        panel.add(north, BorderLayout.NORTH);
        JPanel center = new JPanel(new GridLayout(0,1,10,10));
		
        JButton customer = new JButton();
		customer.setText("Customer");
		customer.setBackground(Color.ORANGE);
		center.add(customer);
		
		JButton product = new JButton();
		product.setText("Product");
		product.setBackground(Color.ORANGE);
		center.add(product);
		
		JButton invoice = new JButton();
		invoice.setText("Invoice");
		invoice.setBackground(Color.ORANGE);
		center.add(invoice);
		
		panel.add(center, BorderLayout.CENTER);
		
		frame.pack();
		frame.getContentPane().add(panel);

		/**
		 * This add an action listener to the customer button. When clicked, frame is disposed and new frame opens
		 * @throws SQLException Thrown when the connection to the database fails.
		 */
		customer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				try {
					new testing();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		
		frame.setSize(600, 400);
		frame.setVisible(true);
		
	}


	public static void main(String[] args) {
		 new MainMenu();

	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}


	

}


