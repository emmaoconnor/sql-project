import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 * The customer login page
 * @author Emma
 */
public class customerLogin extends JFrame {

	JFrame frame;
	JPanel btnpanel, textpanel, label;
	JLabel userLab, passwordLab;
	JTextField userText;
	JPasswordField passwordText;
	JButton submitBtn, cancel;
	
	/**
	 * The constructor creates a simple window.
	 */
	public customerLogin()
	{
		super("Login Autentification");
		setSize(300,200);
		setLocation(500,280);
		setLayout(null);
		
		frame = new JFrame ("Login Form");
		label = new JPanel(new GridLayout(3,2));
		btnpanel = new JPanel();
        
        userLab = new JLabel("Username :");
        passwordLab = new JLabel("Password :");
        submitBtn = new JButton("Login");
        cancel = new JButton("Cancel");
        
        userText = new JTextField(20);
        passwordText = new JPasswordField(6);
        
        /**
    	 * The image icon is inserted on the login page
    	 * @param icon is the image being used for login page
    	 */
        ImageIcon icon = new ImageIcon("D:/login.jpg");
        JLabel lab = new JLabel(icon);
         
        
        label.add(userLab);
        label.add(passwordLab);
        label.add(userText);
        label.add(passwordText);
        btnpanel.add(submitBtn);
        btnpanel.add(cancel);
        
        frame.add(label, BorderLayout.CENTER);
        frame.add(btnpanel, BorderLayout.SOUTH);
        frame.add(lab, BorderLayout.NORTH);
        
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 500);
        frame.setVisible(true);
        
        actionlogin();
		
	}
	
	public void actionlogin()
	{
		/**
		 * The login button is clicked and validation is applied
		 */
		submitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String user = userText.getText();
				String pass = passwordText.getText();
				
				if(user.equals("root") && (pass.trim().equals("carl0w"))) {
					frame.dispose();
					setVisible(false);
					new MainMenu();
					JOptionPane.showMessageDialog(null, "Hello " + user);
				}
				else {
					JOptionPane.showMessageDialog(null, "Invalid Username and password, please try again.");
				}
			}
		});
		
		/**
		 * The text is reset when cancel button is clicked
		 */
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userText.setText("");
				passwordText.setText("");
			}
		});
	}
	
	
	
	public static void main(String[] args) {
		
		 new customerLogin();
		 
	}















	















	

}
