import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import com.mysql.*;

/**
 * The testing window contains final validation of CustomerGUI
 * @author Emma
 */
public class testing extends CustomerGUI{
	
	CustomerGUI customerGUIObject;
	Object[][] data; 
	String[] columnNames = {"CustomerID","First Name","LastName", "Phone", "Address"};
	ResultSet resultSet;
	Connection connection;
	Statement statement;
	ResultSetMetaData metaData;
	DefaultTableModel dm;
	
	@SuppressWarnings("serial")
	/**
	 * The constructor creates a simple window.
	 * @throws SQLException Thrown when the connection to the database fails.
	 */
	public testing() throws SQLException {
		/**
		 * Key listeners and focus listeners are used in text fields
		 */
		
		fName.addFocusListener(new FocusListener(){
            public void focusGained(FocusEvent fe){
            	fName.setBackground(Color.ORANGE);
            }
             
            public void focusLost(FocusEvent fe){
                fName.getText();
                fName.setBackground(Color.WHITE);
            }
        });
		
		fName.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char letter = e.getKeyChar();
				if(fName.getText().length() >= 20)
				{
					e.consume();
	                JOptionPane.showMessageDialog(null, "First name must not exceed 20 characters", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);

				}
				else if(!Character.isLetter(letter)){
					e.consume();
	                JOptionPane.showMessageDialog(null, "Please provide letters for first name e.g. ABC", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);

				}
			}
			
		});
		
		lName.addFocusListener(new FocusListener(){
            public void focusGained(FocusEvent fe){
            	lName.setBackground(Color.ORANGE);
            }
             
            public void focusLost(FocusEvent fe){
                lName.getText();
                lName.setBackground(Color.WHITE);
            }
        });
		
		lName.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char letter = e.getKeyChar();
				if(lName.getText().length()>= 20) 
				{
					e.consume();
					JOptionPane.showMessageDialog(null, "Exceeds required data, please use a shorter name", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);
				}
				else if((!Character.isLetter(letter)))
				{
					e.consume();
					JOptionPane.showMessageDialog(null, "Please provide letters for last name e.g. ABC", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		
		phone.addFocusListener(new FocusListener(){
            public void focusGained(FocusEvent fe){
            	phone.setBackground(Color.ORANGE);
                 
            }
             
            public void focusLost(FocusEvent fe){
                phone.getText();
                phone.setBackground(Color.WHITE);
            }
        });
		
	  phone.addKeyListener(new KeyAdapter() {
      	public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE) || (c == '(') || (c == ')') || (c == '-') || (c == ' ')))
                {
                      e.consume();
                      JOptionPane.showMessageDialog(null, "Please provide numbers for phone number e.g. 1234567891 with brackets, spaces or dashes(-)", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);

                }

                if(phone.getText().length()>11) {

                      e.consume();
                      JOptionPane.showMessageDialog(null, "Exceeds required data, please enter no more than 10 digits", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);

                }
                
            }
      
      });
	  
	  addr.addFocusListener(new FocusListener(){
          
          public void focusLost(FocusEvent fe){
              addr.getText();
              addr.setBackground(Color.WHITE);
          }
           
          public void focusGained(FocusEvent fe){
        	  addr.setBackground(Color.ORANGE);
          }
      });
	  
	  addr.addKeyListener(new KeyAdapter() {
          public void keyTyped(KeyEvent e) { 
        	  char c = e.getKeyChar();
        	  if(addr.getText().length() >= 50)
        	  {
        		  e.consume();
        		  JOptionPane.showMessageDialog(null, "Exceeds required data, please use a shorter address", JOptionPane.MESSAGE_PROPERTY, JOptionPane.WARNING_MESSAGE);
        	  }
        	 
          }  
      });
	  
	  searchfield.addFocusListener(new FocusListener(){
          public void focusGained(FocusEvent fe){
          	searchfield.setBackground(Color.ORANGE);
               
          }
           
          public void focusLost(FocusEvent fe){
              searchfield.getText();
              searchfield.setBackground(Color.WHITE);
          }
      });
       
		
	}	
		

	public static void main(String[] args) throws SQLException {
		new testing();

	}

}
